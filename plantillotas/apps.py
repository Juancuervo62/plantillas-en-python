from django.apps import AppConfig


class PlantillotasConfig(AppConfig):
    name = 'plantillotas'
